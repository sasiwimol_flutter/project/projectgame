import 'package:flutter/material.dart';
import 'package:project_questiongame/answer.dart';

class Gamepage extends StatefulWidget {
  @override
  _GamepageState createState() => _GamepageState();
}

class _GamepageState extends State<Gamepage> {
  List<Icon> _scoreTracker = [];

  int _questionIndex = 0;
  bool answerWasSelected = false;
  int _totalScore = 0;
  bool endOfQuiz = false;

  void _questionAnswer(bool answerScore) {
    setState(() {
      answerWasSelected = true;

      if (answerScore) {
        _totalScore++;
      }
      _scoreTracker.add(
        answerScore
            ? Icon(
                Icons.check_circle,
                color: Colors.green,
              )
            : Icon(
                Icons.clear,
                color: Colors.red,
              ),
      );
      if (_questionIndex + 1 == _questions.length) {
        endOfQuiz = true;
      }
    });
  }

  void _nextQuestion() {
    setState(() {
      _questionIndex++;
      answerWasSelected = false;
    });
    if (_questionIndex >= _questions.length) {
      _resetQuiz();
    }
  }

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
      _scoreTracker = [];
      endOfQuiz = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.purple.shade100,
      body: Center(
        child: Column(
          children: [
            Row(
              children: [
                if (_scoreTracker.length == 0)
                  SizedBox(
                    height: 25.0,
                  ),
                if (_scoreTracker.length > 0) ..._scoreTracker
              ],
            ),
            Container(
              width: double.infinity,
              height: 280.0,
              margin: EdgeInsets.only(bottom: 10.0, left: 30.0, right: 30.0),
              padding: EdgeInsets.symmetric(horizontal: 50.0, vertical: 20.0),
              decoration: BoxDecoration(
                color: Colors.purple.shade100,
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Column(
                children: [
                  Image.network(_questions[_questionIndex]['question'])
                ],
              ),
            ),
            ...(_questions[_questionIndex]['answers']
                    as List<Map<String, dynamic>>)
                .map((answer) {
              return Answer(
                answerText: answer['answerText'],
                answerColor: answerWasSelected
                    ? answer['score']
                        ? Colors.purple.shade300
                        : Colors.red
                    : null,
                answerTap: () {
                  if (answerWasSelected) {
                    return;
                  }
                  _questionAnswer(answer['score']);
                },
              );
            }),
            SizedBox(
              height: 20.0,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                  minimumSize: Size(double.infinity, 40.0)),
              onPressed: () {
                if (!answerWasSelected) {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text(
                        'please select an answer before going to the new question '),
                  ));
                  return;
                }
                _nextQuestion();
              },
              child: Text(endOfQuiz ? 'Restart Quiz' : 'Next'),
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              child: Text(
                '${_totalScore.toString()} / ${_questions.length}',
                style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.purple.shade600),
              ),
            ),
            if (endOfQuiz)
              Container(
                height: 60.0,
                width: 500.0,
                color: Colors.purple.shade200,
                child: Center(
                  child: Text(
                    _totalScore >= 3
                        ? 'Congratulations! Your final score is: $_totalScore'
                        : 'Your final score is: $_totalScore. Better luck next time!',
                    style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                        color: _totalScore >= 3
                            ? Colors.white
                            : Colors.red.shade400),
                  ),
                ),
              )
          ],
        ),
      ),
    );
  }
}

final _questions = const [
  {
    'question':
        'https://dkt6rvnu67rqj.cloudfront.net/cdn/ff/zqkf-jf8giOA7J21vXDDaXoOSvtnxjzFvSUHiWHqVkw/1579043087/public/styles/600x400/public/media/1015142-flip.jpg?h=c51ba6fe&itok=jEsF24iU',
    'answers': [
      {'answerText': 'Tiger', 'score': true},
      {'answerText': 'Dog', 'score': false},
      {'answerText': 'kangaroo', 'score': false},
      {'answerText': 'Cat', 'score': false},
    ],
  },
  {
    'question':
        'https://static.wixstatic.com/media/c58c48_0a6b2aac643a47b0bb08908abe8ffbfb~mv2.jpeg/v1/fill/w_1000,h_667,al_c,q_90,usm_0.66_1.00_0.01/c58c48_0a6b2aac643a47b0bb08908abe8ffbfb~mv2.jpeg',
    'answers': [
      {'answerText': 'seal', 'score': false},
      {'answerText': 'Lion', 'score': false},
      {'answerText': 'cat', 'score': false},
      {'answerText': 'zebra', 'score': true},
    ],
  },
  {
    'question':
        'https://i1.wp.com/chimlang.com/wp-content/uploads/top-8-tallest-animals-in-the-world-1.jpg?fit=500%2C333&ssl=1',
    'answers': [
      {'answerText': 'monkey', 'score': false},
      {'answerText': 'Lion', 'score': false},
      {'answerText': 'giraffe', 'score': true},
      {'answerText': 'turtle', 'score': false},
    ],
  },
  {
    'question':
        'https://cdn.pixabay.com/photo/2020/09/19/21/13/stump-tailed-macaque-5585546_960_720.jpg',
    'answers': [
      {'answerText': 'monkey', 'score': true},
      {'answerText': 'turtle', 'score': false},
      {'answerText': 'dog', 'score': false},
      {'answerText': 'crocodile', 'score': false},
    ],
  },
  {
    'question': 'https://mpics.mgronline.com/pics/Images/564000007398802.JPEG',
    'answers': [
      {'answerText': 'crocodile', 'score': false},
      {'answerText': 'hippopotamus', 'score': true},
      {'answerText': 'monkey', 'score': false},
      {'answerText': 'Dolphin', 'score': false},
    ],
  },
];
